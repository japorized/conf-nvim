# conf-nvim

My personal configuration for neovim

---

## Setup

```
git clone --recurse-submodule --remote-submodules \
  git@gitlab.com:japorized/conf-nvim.git $XDG_CONFIG_HOME/nvim
```

## TODOs

- Completely move some of the items that are still in `~/.vim` over here and be
  sure to update relevant configurations.
- Prepare a bootstrap script that will perform certain installations for certain
  plugins (e.g. LanguageClient-neovim).

<!-- vim: tw=80 -->
