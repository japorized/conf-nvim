filetype plugin on
syntax on
let &packpath=&runtimepath

set background=dark            " For the sake of my eyes
set concealcursor=""           " By default, I don't like having stuff concealed
set conceallevel=2             " Sane concealment level, when there is concealment
set encoding=utf-8             " UTF-8 is everywhere these days
set expandtab                  " spaces > tabs, most of the time
set foldlevelstart=0           " Sane starting fold level
set backspace=indent,eol,start " Sane backspace settings
set fillchars+=|               " Nice filler for separators
set hidden                     " If I don't see the buffer, it doesn't exist
set belloff=all                " No bells please
set incsearch                  " Show all matches when searching
set laststatus=3               " All windows will always have status lines
set ruler                      " Show the row and col of the cursor
set noshowmode                 " Don't show the mode into messages
set showmatch                  " Show matching bracket
set shiftwidth=2               " Number of spaces to use for (auto) indent
set ignorecase                 " Search while ignoring cases
set smartcase                  " Override 'ignorecase' if search has uppercase
set softtabstop=2              " Number of tabs that a <Tab> counts for
set tabstop=2                  " The number of spaces that a <Tab> counts for
set t_Co=256                   " Use 256 colors for the terminal
set timeoutlen=200             " Rather short timeout length for snappiness
set updatetime=1000            " Time taken before the next update redraw happens
set signcolumn=yes             " Always show the sign column
set splitbelow                 " Horizontal splits always go below
set splitright                 " Vertical splits always go to the right
set inccommand=nosplit         " Show substitute changes inline
set mouse=nvi                  " Enable mouse control during normal, visual and insert modes only
set completeopt=menuone,preview,noselect
set shortmess+=c

" Spacebar leader masterrace
map <SPACE> <Nop>
let mapleader=" "

" Workaround for crontab saving problems
au FileType crontab setlocal bkc=yes
if has("autocmd")
  autocmd FileType InsertLeave * update
endif

" quick-scoping
" Trigger a highlight only when pressing f and F.
let g:qs_highlight_on_keys = ['f', 'F', 't', 'T']

function! Helptags() abort
  for g in glob($XDG_CONFIG_HOME . '/nvim/pack/*/*/*/doc', '\n', 1)
      try
        execute 'helptags' g
      catch /.*/
        echom "Failed to generate helptags for " . g
      endtry
  endfor
  echom "Helptags generation process complete!"
endfunction

lua << EOF
-- vim.lsp.set_log_level("debug")
require 'init'
EOF

colorscheme kanagawa

" Jump forward or backward
imap <expr> <Tab>   vsnip#jumpable(1)   ? '<Plug>(vsnip-jump-next)'      : '<Tab>'
smap <expr> <Tab>   vsnip#jumpable(1)   ? '<Plug>(vsnip-jump-next)'      : '<Tab>'
imap <expr> <S-Tab> vsnip#jumpable(-1)  ? '<Plug>(vsnip-jump-prev)'      : '<S-Tab>'
smap <expr> <S-Tab> vsnip#jumpable(-1)  ? '<Plug>(vsnip-jump-prev)'      : '<S-Tab>'

" Expand or jump
imap <expr> <Tab>   vsnip#available(1) ? '<Plug>(vsnip-expand-or-jump)' : '<Tab>'

" Select or cut text to use as $TM_SELECTED_TEXT in the next snippet.
" See https://github.com/hrsh7th/vim-vsnip/pull/50
nmap        s   <Plug>(vsnip-select-text)
xmap        s   <Plug>(vsnip-select-text)
nmap        S   <Plug>(vsnip-cut-text)
xmap        S   <Plug>(vsnip-cut-text)

" tnoremap <esc> <c-\><c-n>
