if exists('syntax_on')
  syntax reset
endif

let g:colors_name = 'deep_purple'

set background=dark
hi Normal           ctermbg=NONE ctermfg=255  cterm=NONE        guibg=NONE    guifg=#fef5fe  gui=NONE

hi Comment          ctermbg=NONE ctermfg=245  cterm=NONE        guibg=NONE    guifg=#928898  gui=ITALIC
hi Statement        ctermbg=NONE ctermfg=161  cterm=NONE        guibg=NONE    guifg=#d93872  gui=NONE
hi Type             ctermbg=NONE ctermfg=199  cterm=BOLD        guibg=NONE    guifg=#ff00af  gui=NONE
hi Function         ctermbg=NONE ctermfg=199  cterm=BOLD        guibg=NONE    guifg=#ff00af  gui=NONE
hi PreProc          ctermbg=NONE ctermfg=199  cterm=BOLD        guibg=NONE    guifg=#ff00af  gui=NONE

hi Title            ctermbg=NONE ctermfg=199  cterm=BOLD        guibg=NONE    guifg=#ff00af  gui=BOLD
hi Special          ctermbg=NONE ctermfg=207  cterm=NONE        guibg=NONE    guifg=#f783f7  gui=NONE
hi Identifier       ctermbg=NONE ctermfg=93   cterm=NONE        guibg=NONE    guifg=#8700ff  gui=NONE
hi Constant         ctermbg=NONE ctermfg=93   cterm=NONE        guibg=NONE    guifg=#8700ff  gui=NONE
hi Boolean          ctermbg=NONE ctermfg=93   cterm=NONE        guibg=NONE    guifg=#8700ff  gui=NONE
hi String           ctermbg=NONE ctermfg=221  cterm=NONE        guibg=NONE    guifg=#ffdb74  gui=NONE
hi Delimiter        ctermbg=NONE ctermfg=135  cterm=NONE        guibg=NONE    guifg=#af5fff  gui=NONE

hi LineNr           ctermbg=NONE ctermfg=207  cterm=NONE        guibg=NONE    guifg=#f783f7  gui=NONE
hi CursorLineNr     ctermbg=NONE ctermfg=221  cterm=NONE        guibg=NONE    guifg=#af5fff  gui=NONE
hi MatchParen       ctermbg=NONE ctermfg=221  cterm=NONE        guibg=NONE    guifg=#af5fff  gui=NONE
hi Conceal          ctermbg=NONE ctermfg=221  cterm=NONE        guibg=NONE    guifg=#af5fff  gui=NONE
hi SpecialKey       ctermbg=NONE ctermfg=207  cterm=NONE        guibg=NONE    guifg=#f783f7  gui=NONE
hi ColorColumn      ctermbg=207  ctermfg=207  cterm=NONE        guibg=#f783f7 guifg=#f783f7  gui=NONE
hi SignColumn       ctermbg=NONE ctermfg=207  cterm=NONE        guibg=NONE    guifg=#f783f7  gui=NONE
hi Folded           ctermbg=NONE ctermfg=93   cterm=NONE        guibg=NONE    guifg=#8700ff  gui=NONE
hi FoldColumn       ctermbg=NONE ctermfg=93   cterm=NONE        guibg=NONE    guifg=#8700ff  gui=NONE

hi Directory        ctermbg=NONE ctermfg=221  cterm=NONE        guibg=NONE    guifg=#af5fff  gui=NONE
hi Underlined       ctermbg=NONE ctermfg=NONE cterm=UNDERLINE   guibg=NONE    guifg=NONE      gui=UNDERLINE

hi Visual           ctermbg=NONE ctermfg=221  cterm=REVERSE     guibg=NONE    guifg=#af5fff gui=REVERSE
hi VisualNOS        ctermbg=NONE ctermfg=NONE cterm=REVERSE     guibg=NONE    guifg=NONE    gui=REVERSE
hi IncSearch        ctermbg=161  ctermfg=235  cterm=NONE        guibg=#d93872 guifg=#262626 gui=NONE
hi Search           ctermbg=221  ctermfg=235  cterm=NONE        guibg=#af5fff guifg=#262626 gui=NONE

hi StatusLine       ctermbg=221  ctermfg=235  cterm=NONE        guibg=#af5fff guifg=#262626 gui=NONE
hi StatusLineNC     ctermbg=199  ctermfg=235  cterm=NONE        guibg=#ff00af guifg=#262626 gui=NONE
hi VertSplit        ctermbg=NONE ctermfg=207  cterm=NONE        guibg=NONE    guifg=#f783f7 gui=NONE
hi WildMenu         ctermbg=255  ctermfg=235  cterm=NONE        guibg=#fef5fe guifg=#262626 gui=NONE
hi ModeMsg          ctermbg=NONE ctermfg=221  cterm=NONE        guibg=NONE    guifg=#af5fff gui=NONE

hi DiffAdd          ctermbg=NONE ctermfg=93   cterm=NONE        guibg=NONE guifg=#8700ff gui=NONE
hi DiffDelete       ctermbg=NONE ctermfg=161  cterm=NONE        guibg=NONE guifg=#d93872 gui=NONE
hi DiffChange       ctermbg=NONE ctermfg=207  cterm=UNDERLINE   guibg=NONE guifg=#f783f7 gui=UNDERLINE
hi DiffText         ctermbg=NONE ctermfg=255  cterm=NONE        guibg=NONE guifg=#fef5fe gui=NONE

hi Pmenu            ctermbg=54   ctermfg=255  cterm=NONE        guibg=#75715e guifg=#fef5fe gui=NONE
hi PmenuSel         ctermbg=161  ctermfg=255  cterm=REVERSE     guibg=#d93872 guifg=#fef5fe gui=REVERSE
hi PmenuSbar        ctermbg=NONE ctermfg=161  cterm=NONE        guibg=NONE    guifg=#d93872 gui=NONE
hi PmenuThumb       ctermbg=93   ctermfg=235  cterm=NONE        guibg=#8700ff guifg=#262626 gui=NONE

hi SpellBad         ctermbg=NONE ctermfg=NONE cterm=UNDERCURL   guibg=NONE    guifg=NONE    gui=UNDERCURL
hi SpellCap         ctermbg=NONE ctermfg=NONE cterm=UNDERCURL   guibg=NONE    guifg=NONE    gui=UNDERCURL
hi SpellLocal       ctermbg=NONE ctermfg=NONE cterm=UNDERCURL   guibg=NONE    guifg=NONE    gui=UNDERCURL
hi SpellRare        ctermbg=NONE ctermfg=NONE cterm=UNDERCURL   guibg=NONE    guifg=NONE    gui=UNDERCURL

hi ErrorMsg         ctermbg=NONE ctermfg=161  cterm=NONE        guibg=#d93872 guifg=#262626 gui=NONE
hi WarningMsg       ctermbg=NONE ctermfg=221  cterm=NONE        guibg=NONE    guifg=#d93872 gui=NONE
hi MoreMsg          ctermbg=NONE ctermfg=207  cterm=NONE        guibg=NONE    guifg=#f783f7 gui=NONE
hi Question         ctermbg=NONE ctermfg=135  cterm=NONE        guibg=NONE    guifg=#af5fff gui=NONE

hi TabLine          ctermbg=NONE ctermfg=255  cterm=NONE        guibg=NONE    guifg=#fef5fe gui=NONE
hi TabLineSel       ctermbg=221  ctermfg=235  cterm=NONE        guibg=#af5fff guifg=#262626 gui=NONE
hi TabLineFill      ctermbg=NONE ctermfg=255  cterm=NONE        guibg=NONE    guifg=#fef5fe gui=NONE

hi Error            ctermbg=NONE ctermfg=161  cterm=REVERSE     guibg=NONE    guifg=#d93872 gui=REVERSE
hi Ignore           ctermbg=NONE ctermfg=NONE cterm=NONE        guibg=NONE    guifg=NONE    gui=NONE
hi Todo             ctermbg=235  ctermfg=93   cterm=UNDERLINE   guibg=#262626 guifg=#8700ff gui=UNDERLINE

hi NonText          ctermbg=NONE ctermfg=207  cterm=NONE        guibg=NONE    guifg=#f783f7 gui=NONE

hi Cursor           ctermbg=255  ctermfg=NONE cterm=NONE        guibg=#fef5fe guifg=NONE    gui=NONE
hi CursorColumn     ctermbg=207  ctermfg=NONE cterm=NONE        guibg=#f783f7 guifg=NONE    gui=NONE
hi CursorLine       ctermbg=207  ctermfg=NONE cterm=NONE        guibg=#f783f7 guifg=NONE    gui=NONE
hi EndOfBuffer      ctermbg=NONE ctermfg=235  cterm=NONE        guibg=NONE    guifg=#262626    gui=NONE

hi helpleadblank    ctermbg=NONE ctermfg=NONE cterm=NONE        guibg=NONE    guifg=NONE    gui=NONE
hi helpnormal       ctermbg=NONE ctermfg=NONE cterm=NONE        guibg=NONE    guifg=NONE    gui=NONE

hi link Number             Constant
hi link Character          Constant

hi link Conditional        Statement
hi link Debug              Special
hi link Define             PreProc
hi link Exception          Statement
hi link Float              Number
hi link HelpCommand        Statement
hi link HelpExample        Statement
hi link Include            PreProc
hi link Keyword            Statement
hi link Label              Statement
hi link Macro              PreProc
hi link Operator           Statement
hi link PreCondit          PreProc
hi link Repeat             Statement
hi link SpecialComment     Delimiter
hi link StorageClass       Type
hi link Structure          Type
hi link Tag                Special
hi link Typedef            Type

hi link htmlEndTag         htmlTagName
hi link htmlLink           Function
hi link htmlSpecialTagName htmlTagName
hi link htmlTag            htmlTagName
hi link xmlTag             Statement
hi link xmlTagName         Statement
hi link xmlEndTag          Statement

hi link markdownItalic     Preproc

hi link pandocDelimitedCodeBlockLanguage    Delimiter
hi link rOKeyword                           SpecialComment

hi link diffBDiffer        WarningMsg
hi link diffCommon         WarningMsg
hi link diffDiffer         WarningMsg
hi link diffIdentical      WarningMsg
hi link diffIsA            WarningMsg
hi link diffNoEOL          WarningMsg
hi link diffOnly           WarningMsg
hi link diffRemoved        WarningMsg
hi link diffAdded          String

hi link vimHiAttrib        Constant
hi link vimParenSep        Normal
hi link vimVar             Normal
hi link vimMapMod          Identifier
hi link vimMapModKey       Identifier
hi link vimNotation        Identifier
hi link vimBracket         Identifier

hi link QuickFixLine       TabLineSel
hi link qfFileName         Function
hi link qfLineNr           String
hi link qfSeparator        Comment

hi ErrorHl      ctermbg=161  ctermfg=235  cterm=UNDERCURL
hi WarningHl    ctermbg=221  ctermfg=235  cterm=UNDERCURL

hi LspDiagnosticsSignError       ctermbg=NONE ctermfg=161 guibg=NONE guifg=#d93872
hi LspDiagnosticsSignWarning     ctermbg=NONE ctermfg=221 guibg=NONE guifg=#e6d700
hi LspDiagnosticsSignInformation ctermbg=NONE ctermfg=207 guibg=NONE guifg=#ff74f1
hi LspDiagnosticsSignHint        ctermbg=NONE ctermfg=93  guibg=NONE guifg=#a353ff
sign define LspDiagnosticsSignError text= texthl=LspDiagnosticsSignError linehl= numhl=
sign define LspDiagnosticsSignWarning text= texthl=LspDiagnosticsSignWarning linehl= numhl=
sign define LspDiagnosticsSignInformation text= texthl=LspDiagnosticsSignInformation linehl= numhl=
sign define LspDiagnosticsSignHint text= texthl=LspDiagnosticsSignHint linehl= numhl=
