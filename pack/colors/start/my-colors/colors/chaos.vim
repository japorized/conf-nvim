if exists('syntax_on')
  syntax reset
endif

let g:colors_name = 'chaos'

if &t_Co >= 256

set background=dark
hi Normal           ctermbg=232  ctermfg=255  cterm=NONE        guibg=#3c4855 guifg=#ebeae9 gui=NONE

hi Comment          ctermbg=232  ctermfg=244  cterm=NONE        guibg=NONE    guifg=#616c72 gui=NONE
hi Statement        ctermbg=232  ctermfg=129  cterm=BOLD        guibg=NONE    guifg=#a5a6a4 gui=NONE
hi Type             ctermbg=232  ctermfg=99   cterm=BOLD        guibg=NONE    guifg=#a5a6a4 gui=NONE
hi Function         ctermbg=232  ctermfg=99   cterm=BOLD        guibg=NONE    guifg=#a5a6a4 gui=NONE
hi PreProc          ctermbg=232  ctermfg=99   cterm=BOLD        guibg=NONE    guifg=#a5a6a4 gui=NONE

hi Title            ctermbg=232  ctermfg=201  cterm=BOLD        guibg=NONE    guifg=#ebeae9 gui=BOLD
hi Special          ctermbg=232  ctermfg=105  cterm=NONE        guibg=NONE    guifg=#a18069 gui=NONE
hi Identifier       ctermbg=232  ctermfg=93   cterm=NONE        guibg=NONE    guifg=#8d8775 gui=NONE
hi Constant         ctermbg=232  ctermfg=93   cterm=NONE        guibg=NONE    guifg=#8d8775 gui=NONE
hi Boolean          ctermbg=232  ctermfg=93   cterm=NONE        guibg=NONE    guifg=#8d8775 gui=NONE
hi String           ctermbg=232  ctermfg=93   cterm=NONE        guibg=NONE    guifg=#8d8775 gui=NONE
hi Delimiter        ctermbg=232  ctermfg=92   cterm=NONE        guibg=NONE    guifg=#899ba6 gui=NONE

hi LineNr           ctermbg=232  ctermfg=105  cterm=NONE        guibg=NONE    guifg=#616c72 gui=NONE
hi CursorLineNr     ctermbg=232  ctermfg=141  cterm=NONE        guibg=NONE    guifg=#ddd668 gui=NONE
hi MatchParen       ctermbg=232  ctermfg=163  cterm=NONE        guibg=NONE    guifg=#3ba2cc gui=NONE
hi Conceal          ctermbg=232  ctermfg=141  cterm=NONE        guibg=NONE    guifg=#616c72 gui=NONE
hi SpecialKey       ctermbg=232  ctermfg=105  cterm=NONE        guibg=NONE    guifg=#616c72 gui=NONE
hi ColorColumn      ctermbg=239  ctermfg=105  cterm=NONE        guibg=#616c72 guifg=#616c72 gui=NONE
hi SignColumn       ctermbg=232  ctermfg=NONE cterm=NONE        guibg=#616c72 guifg=#616c72 gui=NONE
hi Folded           ctermbg=232  ctermfg=127  cterm=NONE        guibg=NONE    guifg=#3ba2cc gui=NONE
hi FoldColumn       ctermbg=232  ctermfg=127  cterm=NONE        guibg=NONE    guifg=#3ba2cc gui=NONE

hi Directory        ctermbg=232  ctermfg=141  cterm=NONE        guibg=NONE    guifg=#5bb899 gui=NONE
hi Underlined       ctermbg=232  ctermfg=NONE cterm=UNDERLINE   guibg=NONE    guifg=NONE    gui=NONE

hi Visual           ctermbg=232  ctermfg=163  cterm=REVERSE     guibg=NONE    guifg=#87d7ff gui=REVERSE
hi VisualNOS        ctermbg=232  ctermfg=NONE cterm=REVERSE     guibg=NONE    guifg=NONE    gui=UNDERLINE
hi IncSearch        ctermbg=91   ctermfg=232  cterm=NONE        guibg=#d5bc02 guifg=#3c4855 gui=NONE
hi Search           ctermbg=91   ctermfg=232  cterm=NONE        guibg=#ddd668 guifg=#3c4855 gui=NONE

hi StatusLine       ctermbg=141  ctermfg=232  cterm=NONE        guibg=#616c72 guifg=#c5d4dd gui=NONE
hi StatusLineNC     ctermbg=141  ctermfg=232  cterm=NONE        guibg=#616c72 guifg=#c5d4dd gui=NONE
hi VertSplit        ctermbg=232  ctermfg=105  cterm=NONE        guibg=NONE    guifg=#616c72 gui=NONE
hi WildMenu         ctermbg=255  ctermfg=232  cterm=NONE        guibg=#c5d4dd guifg=#3c4855 gui=NONE
hi ModeMsg          ctermbg=232  ctermfg=141  cterm=NONE        guibg=NONE    guifg=#5bb899 gui=NONE

hi DiffAdd          ctermbg=232  ctermfg=93   cterm=NONE        guibg=#5bb899 guifg=#3c4855 gui=NONE
hi DiffDelete       ctermbg=232  ctermfg=129  cterm=NONE        guibg=#db6c6c guifg=#3c4855 gui=NONE
hi DiffChange       ctermbg=232  ctermfg=105  cterm=UNDERLINE   guibg=#3c4855 guifg=#d5bc02 gui=UNDERLINE
hi DiffText         ctermbg=232  ctermfg=241  cterm=NONE        guibg=#d5bc02 guifg=#3c4855 gui=NONE

hi Pmenu            ctermbg=244  ctermfg=232  cterm=NONE        guibg=#616c72 guifg=#c5d4dd gui=NONE
hi PmenuSel         ctermbg=129  ctermfg=255  cterm=REVERSE     guibg=#3c4855 guifg=#c5d4dd gui=REVERSE
hi PmenuSbar        ctermbg=232  ctermfg=255  cterm=NONE        guibg=#616c72 guifg=NONE    gui=NONE
hi PmenuThumb       ctermbg=127  ctermfg=232  cterm=NONE        guibg=#c5d4dd guifg=NONE    gui=NONE

hi SpellBad         ctermbg=232  ctermfg=NONE cterm=UNDERCURL   guibg=NONE    guifg=NONE    gui=UNDERCURL
hi SpellCap         ctermbg=232  ctermfg=NONE cterm=UNDERCURL   guibg=NONE    guifg=NONE    gui=UNDERCURL
hi SpellLocal       ctermbg=232  ctermfg=NONE cterm=UNDERCURL   guibg=NONE    guifg=NONE    gui=UNDERCURL
hi SpellRare        ctermbg=232  ctermfg=NONE cterm=UNDERCURL   guibg=NONE    guifg=NONE    gui=UNDERCURL

hi ErrorMsg         ctermbg=232  ctermfg=232  cterm=NONE        guibg=#db6c6c guifg=#616c72 gui=NONE
hi WarningMsg       ctermbg=232  ctermfg=129  cterm=NONE        guibg=NONE    guifg=#db6c6c gui=NONE
hi MoreMsg          ctermbg=232  ctermfg=141  cterm=NONE        guibg=NONE    guifg=#5bb899 gui=NONE
hi Question         ctermbg=232  ctermfg=141  cterm=NONE        guibg=NONE    guifg=#5bb899 gui=NONE

hi TabLine          ctermbg=232  ctermfg=255  cterm=NONE        guibg=#616c72 guifg=#c5d4dd gui=NONE
hi TabLineSel       ctermbg=141  ctermfg=232  cterm=NONE        guibg=#3c4855 guifg=#c5d4dd gui=REVERSE
hi TabLineFill      ctermbg=232  ctermfg=255  cterm=NONE        guibg=#616c72 guifg=#ebeae9 gui=NONE

hi Error            ctermbg=232  ctermfg=129  cterm=REVERSE     guibg=NONE    guifg=#db6c6c gui=REVERSE
hi Ignore           ctermbg=232  ctermfg=NONE cterm=NONE        guibg=NONE    guifg=NONE    gui=NONE
hi Todo             ctermbg=232  ctermfg=127  cterm=UNDERLINE   guibg=#3c4855 guifg=#3ba2cc gui=UNDERLINE

hi NonText          ctermbg=232  ctermfg=105  cterm=NONE        guibg=NONE    guifg=#616c72 gui=NONE

hi Cursor           ctermbg=255  ctermfg=NONE cterm=NONE        guibg=#ebeae9 guifg=NONE    gui=NONE
hi CursorColumn     ctermbg=105  ctermfg=NONE cterm=NONE        guibg=#616c72 guifg=NONE    gui=NONE
hi CursorLine       ctermbg=105  ctermfg=NONE cterm=NONE        guibg=#616c72 guifg=NONE    gui=NONE
hi EndOfBuffer      ctermbg=232  ctermfg=232  cterm=NONE        guibg=NONE    guifg=NONE    gui=NONE

hi helpleadblank    ctermbg=232  ctermfg=NONE cterm=NONE        guibg=NONE    guifg=NONE    gui=NONE
hi helpnormal       ctermbg=232  ctermfg=NONE cterm=NONE        guibg=NONE    guifg=NONE    gui=NONE

endif

hi link Number             Constant
hi link Character          Constant

hi link Conditional        Statement
hi link Debug              Special
hi link Define             PreProc
hi link Exception          Statement
hi link Float              Number
hi link HelpCommand        Statement
hi link HelpExample        Statement
hi link Include            PreProc
hi link Keyword            Statement
hi link Label              Statement
hi link Macro              PreProc
hi link Operator           Statement
hi link PreCondit          PreProc
hi link Repeat             Statement
hi link SpecialComment     Delimiter
hi link StorageClass       Type
hi link Structure          Type
hi link Tag                Special
hi link Typedef            Type

hi link htmlEndTag         htmlTagName
hi link htmlLink           Function
hi link htmlSpecialTagName htmlTagName
hi link htmlTag            htmlTagName
hi link xmlTag             Statement
hi link xmlTagName         Statement
hi link xmlEndTag          Statement

hi link markdownItalic     Preproc

hi link pandocDelimitedCodeBlockLanguage    Delimiter
hi link rOKeyword                           SpecialComment

hi link diffBDiffer        WarningMsg
hi link diffCommon         WarningMsg
hi link diffDiffer         WarningMsg
hi link diffIdentical      WarningMsg
hi link diffIsA            WarningMsg
hi link diffNoEOL          WarningMsg
hi link diffOnly           WarningMsg
hi link diffRemoved        WarningMsg
hi link diffAdded          String

hi link vimHiAttrib        Constant
hi link vimParenSep        Normal
hi link vimVar             Normal
hi link vimMapMod          Identifier
hi link vimMapModKey       Identifier
hi link vimNotation        Identifier
hi link vimBracket         Identifier

hi link QuickFixLine       TabLineSel
hi link qfFileName         Function
hi link qfLineNr           String
hi link qfSeparator        Comment

" vim-moodo
hi MoodoNormal      ctermbg=99  ctermfg=232 cterm=NONE    guibg=#70ef9b   guifg=#2d2d2d   gui=NONE
hi MoodoInsert      ctermbg=105 ctermfg=232 cterm=NONE    guibg=#70c3ff   guifg=#2d2d2d   gui=NONE
hi MoodoVisual      ctermbg=93  ctermfg=232 cterm=NONE    guibg=#34a9d1   guifg=#2d2d2d   gui=NONE
hi MoodoSelect      ctermbg=141 ctermfg=232 cterm=NONE    guibg=#ead637   guifg=#2d2d2d   gui=NONE
hi MoodoReplace     ctermbg=129 ctermfg=232 cterm=NONE    guibg=#fc3737   guifg=#2d2d2d   gui=NONE
hi MoodoCommand     ctermbg=165 ctermfg=232 cterm=NONE    guibg=#8d66ff   guifg=#2d2d2d   gui=NONE
hi MoodoMore        ctermbg=165 ctermfg=232 cterm=NONE    guibg=#8d66ff   guifg=#2d2d2d   gui=NONE
hi MoodoTerminal    ctermbg=91  ctermfg=232 cterm=NONE    guibg=#cf2e2e   guifg=#2d2d2d   gui=NONE
hi MoodoTabLine     ctermbg=232 ctermfg=99  cterm=NONE    guibg=#70ef9b   guifg=#2d2d2d   gui=NONE
hi MoodoTabLineSel  ctermbg=99  ctermfg=232 cterm=NONE    guibg=#2d2d2d   guifg=#ededed   gui=NONE
hi MoodoTabLineFill ctermbg=232 ctermfg=99  cterm=NONE    guibg=#70ef9b   guifg=#2d2d2d   gui=NONE
