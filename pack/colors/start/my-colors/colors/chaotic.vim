if exists('syntax_on')
  syntax reset
endif

let g:colors_name = 'chaotic'

if &t_Co >= 256

set background=dark
hi Normal           ctermbg=234  ctermfg=254  cterm=NONE        guibg=#1c1c1c guifg=#eeeeee gui=NONE

hi Comment          ctermbg=NONE ctermfg=244  cterm=NONE        guibg=NONE    guifg=#808080 gui=NONE
hi Statement        ctermbg=NONE ctermfg=141  cterm=BOLD        guibg=NONE    guifg=#af87ff gui=BOLD
hi Type             ctermbg=NONE ctermfg=105  cterm=BOLD        guibg=NONE    guifg=#8787ff gui=BOLD
hi Function         ctermbg=NONE ctermfg=105  cterm=BOLD        guibg=NONE    guifg=#8787ff gui=BOLD
hi PreProc          ctermbg=NONE ctermfg=105  cterm=BOLD        guibg=NONE    guifg=#8787ff gui=BOLD

hi Title            ctermbg=NONE ctermfg=57   cterm=BOLD        guibg=NONE    guifg=#5f00ff gui=BOLD
hi Special          ctermbg=NONE ctermfg=147  cterm=NONE        guibg=NONE    guifg=#afafff gui=NONE
hi Identifier       ctermbg=NONE ctermfg=135  cterm=NONE        guibg=NONE    guifg=#af5fff gui=NONE
hi Constant         ctermbg=NONE ctermfg=99   cterm=NONE        guibg=NONE    guifg=#875fff gui=NONE
hi Boolean          ctermbg=NONE ctermfg=99   cterm=NONE        guibg=NONE    guifg=#875fff gui=NONE
hi String           ctermbg=NONE ctermfg=213  cterm=NONE        guibg=NONE    guifg=#ff87ff gui=NONE
hi Delimiter        ctermbg=NONE ctermfg=97   cterm=NONE        guibg=NONE    guifg=#875faf gui=NONE

hi LineNr           ctermbg=NONE ctermfg=244  cterm=NONE        guibg=NONE    guifg=#808080 gui=NONE
hi CursorLineNr     ctermbg=NONE ctermfg=147  cterm=NONE        guibg=NONE    guifg=#afafff gui=NONE
hi MatchParen       ctermbg=NONE ctermfg=61   cterm=NONE        guibg=NONE    guifg=#5f5faf gui=NONE
hi Conceal          ctermbg=NONE ctermfg=140  cterm=NONE        guibg=NONE    guifg=#af87d7 gui=NONE
hi SpecialKey       ctermbg=NONE ctermfg=147  cterm=NONE        guibg=NONE    guifg=#afafff gui=NONE
hi ColorColumn      ctermbg=60   ctermfg=147  cterm=NONE        guibg=#5f5f87 guifg=#afafff gui=NONE
hi SignColumn       ctermbg=NONE ctermfg=NONE cterm=NONE        guibg=NONE    guifg=NONE    gui=NONE
hi Folded           ctermbg=NONE ctermfg=244  cterm=NONE        guibg=NONE    guifg=#808080 gui=NONE
hi FoldColumn       ctermbg=NONE ctermfg=244  cterm=NONE        guibg=NONE    guifg=#808080 gui=NONE

hi Directory        ctermbg=NONE ctermfg=140  cterm=NONE        guibg=NONE    guifg=#af87d7 gui=NONE
hi Underlined       ctermbg=NONE ctermfg=NONE cterm=UNDERLINE   guibg=NONE    guifg=NONE    gui=NONE

hi Visual           ctermbg=240  ctermfg=NONE cterm=NONE        guibg=#585858 guifg=NONE    gui=NONE
hi VisualNOS        ctermbg=NONE ctermfg=NONE cterm=REVERSE     guibg=NONE    guifg=NONE    gui=REVERSE
hi IncSearch        ctermbg=96   ctermfg=234  cterm=NONE        guibg=#875f87 guifg=#1c1c1c gui=NONE
hi Search           ctermbg=140  ctermfg=234  cterm=NONE        guibg=#af87d7 guifg=#1c1c1c gui=NONE

hi StatusLine       ctermbg=140  ctermfg=234  cterm=NONE        guibg=#af87d7 guifg=#1c1c1c gui=NONE
hi StatusLineNC     ctermbg=105  ctermfg=234  cterm=NONE        guibg=#8787ff guifg=#1c1c1c gui=NONE
hi VertSplit        ctermbg=NONE ctermfg=147  cterm=NONE        guibg=NONE    guifg=#515151 gui=NONE
hi WildMenu         ctermbg=238  ctermfg=254  cterm=NONE        guibg=#eeeeee guifg=#1c1c1c gui=NONE
hi ModeMsg          ctermbg=NONE ctermfg=140  cterm=NONE        guibg=NONE    guifg=#af87d7 gui=NONE

hi DiffAdd          ctermbg=NONE ctermfg=120  cterm=NONE        guibg=NONE    guifg=#87ff87 gui=NONE
hi DiffDelete       ctermbg=NONE ctermfg=196  cterm=NONE        guibg=NONE    guifg=#ff0000 gui=NONE
hi DiffChange       ctermbg=NONE ctermfg=75   cterm=NONE        guibg=NONE    guifg=#5fafff gui=NONE
hi DiffText         ctermbg=NONE ctermfg=NONE cterm=NONE        guibg=NONE    guifg=NONE    gui=NONE

hi Pmenu            ctermbg=238 ctermfg=254  cterm=NONE        guibg=#4b4b49 guifg=#e4e4e4 gui=NONE
hi PmenuSel         ctermbg=238 ctermfg=254  cterm=REVERSE     guibg=#4b4b49 guifg=#e4e4e4 gui=REVERSE
hi PmenuSbar        ctermbg=234 ctermfg=254  cterm=NONE        guibg=#1c1c1c guifg=#e4e4e4 gui=NONE
hi PmenuThumb       ctermbg=54  ctermfg=234  cterm=NONE        guibg=#5f0087 guifg=#1c1c1c gui=NONE

hi SpellBad         ctermbg=NONE ctermfg=NONE cterm=UNDERCURL   guibg=NONE    guifg=NONE    gui=UNDERCURL
hi SpellCap         ctermbg=NONE ctermfg=NONE cterm=UNDERCURL   guibg=NONE    guifg=NONE    gui=UNDERCURL
hi SpellLocal       ctermbg=NONE ctermfg=NONE cterm=UNDERCURL   guibg=NONE    guifg=NONE    gui=UNDERCURL
hi SpellRare        ctermbg=NONE ctermfg=NONE cterm=UNDERCURL   guibg=NONE    guifg=NONE    gui=UNDERCURL

hi ErrorMsg         ctermbg=196  ctermfg=234  cterm=NONE        guibg=#ff0000 guifg=#1c1c1c gui=NONE
hi WarningMsg       ctermbg=226  ctermfg=234  cterm=NONE        guibg=#ffff5f guifg=#1c1c1c gui=NONE
hi MoreMsg          ctermbg=NONE ctermfg=140  cterm=NONE        guibg=NONE    guifg=#af87d7 gui=NONE
hi Question         ctermbg=NONE ctermfg=140  cterm=NONE        guibg=NONE    guifg=#af87d7 gui=NONE

hi TabLine          ctermbg=NONE ctermfg=98   cterm=NONE        guibg=NONE    guifg=#875fd7 gui=NONE
hi TabLineSel       ctermbg=98   ctermfg=234  cterm=NONE        guibg=#875fd7 guifg=#1c1c1c gui=NONE
hi TabLineFill      ctermbg=NONE ctermfg=98   cterm=NONE        guibg=NONE    guifg=#875fd7 gui=NONE

hi Error            ctermbg=NONE ctermfg=196  cterm=REVERSE     guibg=NONE    guifg=#ff0000 gui=REVERSE
hi Ignore           ctermbg=NONE ctermfg=NONE cterm=NONE        guibg=NONE    guifg=NONE    gui=NONE
hi Todo             ctermbg=NONE ctermfg=54   cterm=UNDERLINE   guibg=NONE    guifg=#5f0087 gui=UNDERLINE

hi NonText          ctermbg=NONE ctermfg=234  cterm=NONE        guibg=NONE    guifg=#1c1c1c gui=NONE

hi Cursor           ctermbg=254  ctermfg=NONE cterm=NONE        guibg=#e4e4e4 guifg=NONE    gui=NONE
hi CursorColumn     ctermbg=147  ctermfg=NONE cterm=NONE        guibg=#afafff guifg=NONE    gui=NONE
hi CursorLine       ctermbg=147  ctermfg=NONE cterm=NONE        guibg=#afafff guifg=NONE    gui=NONE
" hi EndOfBuffer      ctermbg=NONE ctermfg=234   cterm=NONE        guibg=NONE    guifg=NONE    gui=NONE

hi helpleadblank    ctermbg=NONE ctermfg=NONE cterm=NONE        guibg=NONE    guifg=NONE    gui=NONE
hi helpnormal       ctermbg=NONE ctermfg=NONE cterm=NONE        guibg=NONE    guifg=NONE    gui=NONE

endif

hi link Number             Constant
hi link Character          Constant

hi link Conditional        Statement
hi link Debug              Special
hi link Define             PreProc
hi link Exception          Statement
hi link Float              Number
hi link HelpCommand        Statement
hi link HelpExample        Statement
hi link Include            PreProc
hi link Keyword            Statement
hi link Label              Statement
hi link Macro              PreProc
hi link Operator           Statement
hi link PreCondit          PreProc
hi link Repeat             Statement
hi link SpecialComment     Delimiter
hi link StorageClass       Type
hi link Structure          Type
hi link Tag                Special
hi link Typedef            Type

hi link htmlEndTag         htmlTagName
hi link htmlLink           Function
hi link htmlSpecialTagName htmlTagName
hi link htmlTag            htmlTagName
hi link xmlTag             Statement
hi link xmlTagName         Statement
hi link xmlEndTag          Statement

hi link markdownItalic     Preproc

hi link pandocDelimitedCodeBlockLanguage    Delimiter
hi link rOKeyword                           SpecialComment

hi link diffBDiffer        WarningMsg
hi link diffCommon         WarningMsg
hi link diffDiffer         WarningMsg
hi link diffIdentical      WarningMsg
hi link diffIsA            WarningMsg
hi link diffNoEOL          WarningMsg
hi link diffOnly           WarningMsg
hi link diffRemoved        WarningMsg
hi link diffAdded          String

hi link vimHiAttrib        Constant
hi link vimParenSep        Normal
hi link vimVar             Normal
hi link vimMapMod          Identifier
hi link vimMapModKey       Identifier
hi link vimNotation        Identifier
hi link vimBracket         Identifier

hi link QuickFixLine       TabLineSel
hi link qfFileName         Function
hi link qfLineNr           String
hi link qfSeparator        Comment

" vim-moodo
hi MoodoNormal      ctermbg=NONE ctermfg=141  cterm=REVERSE   guibg=#1c1c1c  guifg=#af87ff  gui=REVERSE
hi MoodoInsert      ctermbg=NONE ctermfg=63   cterm=REVERSE   guibg=#1c1c1c  guifg=#5f5fff  gui=REVERSE
hi MoodoVisual      ctermbg=NONE ctermfg=221  cterm=REVERSE   guibg=#1c1c1c  guifg=#ffd75f  gui=REVERSE
hi MoodoSelect      ctermbg=NONE ctermfg=168  cterm=REVERSE   guibg=#1c1c1c  guifg=#d75f87  gui=REVERSE
hi MoodoReplace     ctermbg=NONE ctermfg=207  cterm=REVERSE   guibg=#1c1c1c  guifg=#ff61fe  gui=REVERSE
hi MoodoCommand     ctermbg=NONE ctermfg=99   cterm=REVERSE   guibg=#1c1c1c  guifg=#875fff  gui=REVERSE
hi MoodoMore        ctermbg=NONE ctermfg=177  cterm=REVERSE   guibg=#1c1c1c  guifg=#d787ff  gui=REVERSE
hi MoodoTerminal    ctermbg=NONE ctermfg=56   cterm=REVERSE   guibg=#1c1c1c  guifg=#5f00d7  gui=REVERSE
" hi MoodoTabLine     ctermbg=NONE ctermfg=98   cterm=NONE      guibg=#1c1c1c  guifg=#875fd7  gui=NONE
" hi MoodoTabLineSel  ctermbg=98   ctermfg=234  cterm=NONE      guibg=#875fd7  guifg=#1c1c1c  gui=NONE
" hi MoodoTabLineFill ctermbg=NONE ctermfg=98   cterm=NONE      guibg=#1c1c1c  guifg=#875fd7  gui=NONE
hi link MoodoTabLine TabLine
hi link MoodoTabLineSel TabLineSel
hi link MoodoTabLineFill TabLineFill
