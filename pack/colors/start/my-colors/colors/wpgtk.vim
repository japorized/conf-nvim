if exists('syntax_on')
  syntax reset
endif

let g:colors_name = 'wpgtk'

if &t_Co >= 256

set background=dark
hi Normal           ctermbg=NONE ctermfg=15   cterm=NONE        guibg=NONE    guifg=#f9f8f5  gui=NONE

hi Comment          ctermbg=NONE ctermfg=7    cterm=NONE        guibg=NONE    guifg=#91918e  gui=ITALIC
hi Statement        ctermbg=NONE ctermfg=1    cterm=BOLD        guibg=NONE    guifg=#f92672  gui=NONE
hi Type             ctermbg=NONE ctermfg=2    cterm=BOLD        guibg=NONE    guifg=#a6e22e  gui=NONE
hi Function         ctermbg=NONE ctermfg=2    cterm=BOLD        guibg=NONE    guifg=#a6e22e  gui=NONE
hi PreProc          ctermbg=NONE ctermfg=2    cterm=BOLD        guibg=NONE    guifg=#a6e22e  gui=NONE

hi Title            ctermbg=NONE ctermfg=10   cterm=BOLD        guibg=NONE    guifg=#a6e22e  gui=BOLD
hi Special          ctermbg=NONE ctermfg=4    cterm=NONE        guibg=NONE    guifg=#66d9ef  gui=NONE
hi Identifier       ctermbg=NONE ctermfg=6    cterm=NONE        guibg=NONE    guifg=#a1efe4  gui=NONE
hi Constant         ctermbg=NONE ctermfg=6    cterm=NONE        guibg=NONE    guifg=#a1efe4  gui=NONE
hi Boolean          ctermbg=NONE ctermfg=6    cterm=NONE        guibg=NONE    guifg=#a1efe4  gui=NONE
hi String           ctermbg=NONE ctermfg=6    cterm=NONE        guibg=NONE    guifg=#a1efe4  gui=NONE
hi Delimiter        ctermbg=NONE ctermfg=13   cterm=NONE        guibg=NONE    guifg=#ae81ff  gui=NONE

hi LineNr           ctermbg=NONE ctermfg=4    cterm=NONE        guibg=NONE      guifg=#66d9ef  gui=NONE
hi CursorLineNr     ctermbg=NONE ctermfg=3    cterm=NONE        guibg=NONE      guifg=#f4bf75  gui=NONE
hi MatchParen       ctermbg=NONE ctermfg=11   cterm=NONE        guibg=NONE      guifg=#f4bf75  gui=NONE
hi Conceal          ctermbg=NONE ctermfg=3    cterm=NONE        guibg=NONE      guifg=#f4bf75  gui=NONE
hi SpecialKey       ctermbg=NONE ctermfg=4    cterm=NONE        guibg=NONE      guifg=#66d9ef  gui=NONE
hi ColorColumn      ctermbg=12   ctermfg=4    cterm=NONE        guibg=#66d9ef guifg=#66d9ef  gui=NONE
hi SignColumn       ctermbg=NONE ctermfg=4    cterm=NONE        guibg=NONE      guifg=#66d9ef  gui=NONE
hi Folded           ctermbg=NONE ctermfg=14   cterm=NONE        guibg=NONE      guifg=#a1efe4  gui=NONE
hi FoldColumn       ctermbg=NONE ctermfg=14   cterm=NONE        guibg=NONE      guifg=#a1efe4  gui=NONE

hi Directory        ctermbg=NONE ctermfg=3    cterm=NONE        guibg=NONE    guifg=#f4bf75  gui=NONE
hi Underlined       ctermbg=NONE ctermfg=NONE cterm=UNDERLINE   guibg=NONE    guifg=NONE      gui=UNDERLINE

hi Visual           ctermbg=NONE ctermfg=11   cterm=REVERSE     guibg=NONE    guifg=#f4bf75 gui=REVERSE
hi VisualNOS        ctermbg=NONE ctermfg=NONE cterm=REVERSE     guibg=NONE    guifg=NONE    gui=REVERSE
hi IncSearch        ctermbg=9    ctermfg=0    cterm=NONE        guibg=#f92672 guifg=#272822 gui=NONE
hi Search           ctermbg=3    ctermfg=0    cterm=NONE        guibg=#f4bf75 guifg=#272822 gui=NONE

hi StatusLine       ctermbg=3    ctermfg=0    cterm=NONE        guibg=#f4bf75 guifg=#272822 gui=NONE
hi StatusLineNC     ctermbg=2    ctermfg=0    cterm=NONE        guibg=#a6e22e guifg=#272822 gui=NONE
hi VertSplit        ctermbg=NONE ctermfg=4    cterm=NONE        guibg=NONE    guifg=#66d9ef gui=NONE
hi WildMenu         ctermbg=15   ctermfg=0    cterm=NONE        guibg=#f9f8f5 guifg=#272822 gui=NONE
hi ModeMsg          ctermbg=NONE ctermfg=3    cterm=NONE        guibg=NONE    guifg=#f4bf75 gui=NONE

hi DiffAdd          ctermbg=NONE ctermfg=6    cterm=NONE        guibg=NONE guifg=#a1efe4 gui=NONE
hi DiffDelete       ctermbg=NONE ctermfg=1    cterm=NONE        guibg=NONE guifg=#f92672 gui=NONE
hi DiffChange       ctermbg=NONE ctermfg=4    cterm=UNDERLINE   guibg=NONE guifg=#66d9ef gui=UNDERLINE
hi DiffText         ctermbg=NONE ctermfg=15   cterm=NONE        guibg=NONE guifg=#f9f8f5 gui=NONE

hi Pmenu            ctermbg=8    ctermfg=15   cterm=NONE        guibg=#75715e guifg=#f9f8f5 gui=NONE
hi PmenuSel         ctermbg=1    ctermfg=15   cterm=REVERSE     guibg=#f92672 guifg=#f9f8f5 gui=REVERSE
hi PmenuSbar        ctermbg=NONE ctermfg=1    cterm=NONE        guibg=NONE     guifg=#f92672    gui=NONE
hi PmenuThumb       ctermbg=14   ctermfg=0    cterm=NONE        guibg=#a1efe4 guifg=#272822    gui=NONE

hi SpellBad         ctermbg=NONE ctermfg=NONE cterm=UNDERCURL   guibg=NONE    guifg=NONE    gui=UNDERCURL
hi SpellCap         ctermbg=NONE ctermfg=NONE cterm=UNDERCURL   guibg=NONE    guifg=NONE    gui=UNDERCURL
hi SpellLocal       ctermbg=NONE ctermfg=NONE cterm=UNDERCURL   guibg=NONE    guifg=NONE    gui=UNDERCURL
hi SpellRare        ctermbg=NONE ctermfg=NONE cterm=UNDERCURL   guibg=NONE    guifg=NONE    gui=UNDERCURL

hi ErrorMsg         ctermbg=NONE ctermfg=1    cterm=NONE        guibg=#f92672 guifg=#272822 gui=NONE
hi WarningMsg       ctermbg=NONE ctermfg=3    cterm=NONE        guibg=NONE    guifg=#f92672 gui=NONE
hi MoreMsg          ctermbg=NONE ctermfg=4    cterm=NONE        guibg=NONE    guifg=#f4bf75 gui=NONE
hi Question         ctermbg=NONE ctermfg=5    cterm=NONE        guibg=NONE    guifg=#f4bf75 gui=NONE

hi TabLine          ctermbg=NONE ctermfg=15   cterm=NONE        guibg=NONE    guifg=#f9f8f5 gui=NONE
hi TabLineSel       ctermbg=3    ctermfg=0    cterm=NONE        guibg=#f4bf75 guifg=#272822 gui=NONE
hi TabLineFill      ctermbg=NONE ctermfg=15   cterm=NONE        guibg=NONE    guifg=#f9f8f5 gui=NONE

hi Error            ctermbg=NONE ctermfg=1    cterm=REVERSE     guibg=NONE    guifg=#f92672 gui=REVERSE
hi Ignore           ctermbg=NONE ctermfg=NONE cterm=NONE        guibg=NONE    guifg=NONE    gui=NONE
hi Todo             ctermbg=0    ctermfg=14   cterm=UNDERLINE   guibg=#272822 guifg=#a1efe4 gui=UNDERLINE

hi NonText          ctermbg=NONE ctermfg=4    cterm=NONE        guibg=NONE    guifg=#66d9ef gui=NONE

hi Cursor           ctermbg=15   ctermfg=NONE cterm=NONE        guibg=#f9f8f5 guifg=NONE    gui=NONE
hi CursorColumn     ctermbg=4    ctermfg=NONE cterm=NONE        guibg=#66d9ef guifg=NONE    gui=NONE
hi CursorLine       ctermbg=4    ctermfg=NONE cterm=NONE        guibg=#66d9ef guifg=NONE    gui=NONE
hi EndOfBuffer      ctermbg=NONE ctermfg=0    cterm=NONE        guibg=NONE    guifg=#272822    gui=NONE

hi helpleadblank    ctermbg=NONE ctermfg=NONE cterm=NONE        guibg=NONE    guifg=NONE    gui=NONE
hi helpnormal       ctermbg=NONE ctermfg=NONE cterm=NONE        guibg=NONE    guifg=NONE    gui=NONE

endif

hi link Number             Constant
hi link Character          Constant

hi link Conditional        Statement
hi link Debug              Special
hi link Define             PreProc
hi link Exception          Statement
hi link Float              Number
hi link HelpCommand        Statement
hi link HelpExample        Statement
hi link Include            PreProc
hi link Keyword            Statement
hi link Label              Statement
hi link Macro              PreProc
hi link Operator           Statement
hi link PreCondit          PreProc
hi link Repeat             Statement
hi link SpecialComment     Delimiter
hi link StorageClass       Type
hi link Structure          Type
hi link Tag                Special
hi link Typedef            Type

hi link htmlEndTag         htmlTagName
hi link htmlLink           Function
hi link htmlSpecialTagName htmlTagName
hi link htmlTag            htmlTagName
hi link xmlTag             Statement
hi link xmlTagName         Statement
hi link xmlEndTag          Statement

hi link markdownItalic     Preproc

hi link pandocDelimitedCodeBlockLanguage    Delimiter
hi link rOKeyword                           SpecialComment

hi link diffBDiffer        WarningMsg
hi link diffCommon         WarningMsg
hi link diffDiffer         WarningMsg
hi link diffIdentical      WarningMsg
hi link diffIsA            WarningMsg
hi link diffNoEOL          WarningMsg
hi link diffOnly           WarningMsg
hi link diffRemoved        WarningMsg
hi link diffAdded          String

hi link vimHiAttrib        Constant
hi link vimParenSep        Normal
hi link vimVar             Normal
hi link vimMapMod          Identifier
hi link vimMapModKey       Identifier
hi link vimNotation        Identifier
hi link vimBracket         Identifier

hi link QuickFixLine       TabLineSel
hi link qfFileName         Function
hi link qfLineNr           String
hi link qfSeparator        Comment

hi ErrorHl      ctermbg=1    ctermfg=0    cterm=UNDERCURL
hi WarningHl    ctermbg=3    ctermfg=0    cterm=UNDERCURL

hi LspDiagnosticsSignError       ctermbg=NONE ctermfg=1   guibg=NONE guifg=#ff0000
hi LspDiagnosticsSignWarning     ctermbg=NONE ctermfg=3   guibg=NONE guifg=#00ffff
hi LspDiagnosticsSignInformation ctermbg=NONE ctermfg=4   guibg=NONE guifg=#0000ff
hi LspDiagnosticsSignHint        ctermbg=NONE ctermfg=6   guibg=NONE guifg=#ff00ff
sign define LspDiagnosticsSignError text= texthl=LspDiagnosticsSignError linehl= numhl=
sign define LspDiagnosticsSignWarning text= texthl=LspDiagnosticsSignWarning linehl= numhl=
sign define LspDiagnosticsSignInformation text= texthl=LspDiagnosticsSignInformation linehl= numhl=
sign define LspDiagnosticsSignHint text= texthl=LspDiagnosticsSignHint linehl= numhl=
