let g:moodo_modified_indicator = "*"
let g:moodo_tabline_closestr = ''
let g:moodo_left = "\ \ %#MoodoMore#%(\ %{ShowGitBranch()}\ %)%#Normal#"
let g:moodo_mid = "\  %r%h\ %.80f%(\ %q%)\ %(\ %#MoodoReplace#%{&modified?' '.g:moodo_modified_indicator.' ':''}%#Normal# %)\ "
let g:moodo_right = "%(%{moodo#MoodoFT()}%) %l/%L\ \ %2c\ \ "
let g:moodo_modes = {
  \ 'n'      : '',
  \ 'no'     : '',
  \ 'v'      : '󰒅',
  \ 'V'      : '󰒅',
  \ "\<C-V>" : '󰒅',
  \ 's'      : '󰗅',
  \ 'S'      : '󰗅',
  \ "\<C-S>" : '󰗅',
  \ 'i'      : '󰏫',
  \ 'ic'     : '󰏫',
  \ 'ix'     : '󰏫',
  \ 'R'      : '',
  \ 'Rc'     : '',
  \ 'Rv'     : '',
  \ 'Rx'     : '',
  \ 'c'      : '',
  \ 'cv'     : '',
  \ 'ce'     : '',
  \ 'r'      : '',
  \ 'rm'     : '',
  \ '!'      : '',
  \ 't'      : '',
  \ }
let g:moodo_fticons = {
  \ 'conf'       : '',
  \ 'fugitive'   : '',
  \ 'help'       : '',
  \ 'magit'      : '',
  \ 'markdown'   : '',
  \ 'simplenote' : 'ﴬ',
  \ 'startify'   : '',
  \ 'vim'        : '',
  \ }

function! ShowGitBranch() abort
  let l:git_branch = FugitiveHead(7)
  if l:git_branch != ''
    return '  ' .. l:git_branch .. ' '
  else
    return ''
  endif
endfunction
