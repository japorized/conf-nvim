let g:VM_default_mappings = 0
let g:VM_maps = {}
let g:VM_maps = {
      \ "Exit": "<Esc>",
      \ "Find Under": "<C-n>",
      \ "Find Subword Under": "<C-n>",
      \ "Add Cursor Down": "<C-Down>",
      \ "Add Cursor Up": "<C-Up>",
      \ "Select All": "<M-C-n>",
      \ "Start Regex Search": "<M-C-/>",
      \ "Mouse Cursor": "<C-LeftMouse>",
      \ "Mouse Word": "<C-RightMouse>",
      \ "Mouse Column": "<M-C-RightMouse>",
      \ "Find Next": "<C-n>",
      \ "Find Prev": "<C-p>",
      \ "Goto Next": "]",
      \ "Goto Prev": "[",
      \ "Skip Region": "<C-x>",
      \ }
