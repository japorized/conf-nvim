autocmd BufRead,BufNewFile *.cs setlocal shiftwidth=4
autocmd BufRead,BufNewFile *.cs setlocal softtabstop=1
autocmd BufRead,BufNewFile *.cs setlocal tabstop=4
