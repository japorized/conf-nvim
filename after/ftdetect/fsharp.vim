autocmd BufRead,BufNewFile *.fs,*.fsx setlocal shiftwidth=4
autocmd BufRead,BufNewFile *.fs,*.fsx setlocal softtabstop=1
autocmd BufRead,BufNewFile *.fs,*.fsx setlocal tabstop=4
autocmd BufRead,BufNewFile *.fs,*.fsx setlocal textwidth=0
autocmd FileType fsharp setlocal commentstring=//\ %s
