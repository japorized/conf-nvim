autocmd BufRead,BufNewFile *.go setlocal noexpandtab
autocmd BufRead,BufNewFile *.go setlocal shiftwidth=2
autocmd BufRead,BufNewFile *.go setlocal softtabstop=1
autocmd BufRead,BufNewFile *.go setlocal tabstop=2
