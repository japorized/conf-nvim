autocmd BufRead,BufNewFile *.sql setlocal shiftwidth=4
autocmd BufRead,BufNewFile *.sql setlocal softtabstop=4
autocmd BufRead,BufNewFile *.sql setlocal tabstop=4
