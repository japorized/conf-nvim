function ToggleTypewriterMode()
  if (not (vim.o.scrolloff == 999)) then
    vim.o.scrolloff = 999
  else
    vim.o.scrolloff = 0
  end
end

function ToggleLineNumbers()
  vim.o.relativenumber = false
  vim.o.number = not vim.o.number
end

function ToggleRelativeNumbers()
  vim.o.number = true
  vim.o.relativenumber = not vim.o.relativenumber
end
