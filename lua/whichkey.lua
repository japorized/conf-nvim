require 'useful_functions'
local telescope = require 'telescope'
local telescope_builtin = require 'telescope.builtin'
local telescope_actions = require 'telescope.actions'
local dap = require 'dap'
local dap_widgets = require 'dap.ui.widgets'
local whichkey = require 'which-key'

vim.keymap.set('i', '<C-J>', 'copilot#Accept("\\<CR>")', { expr = true; replace_keycodes = false })
vim.g.copilot_no_tab_map = true

telescope.setup {
  defaults = {
    mappings = {
      i = {
        ["<C-h>"] = "which_key",
        ["<C-x>"] = false,
        ["<C-s>"] = telescope_actions.select_horizontal,
        ["<Up>"] = false,
        ["<Down>"] = false,
      },
      n = {
        ["<C-h>"] = "which_key",
        ["<C-s>"] = telescope_actions.select_horizontal,
        ["<C-v>"] = telescope_actions.select_vertical,
      },
    }
  },
  pickers = {
    buffers = {
      mappings = {
        n = {
          ["D"] = telescope_actions.delete_buffer,
        },
      },
    },
    treesitter = {
      mappings = {
        i = {
          ["<C-l>"] = false,
          ["<C-n>"] = telescope_actions.complete_tag,
        }
      }
    }
  }
}

-- normal mode keybinds
whichkey.add({
  { "<leader>/", function() telescope_builtin.live_grep({ hidden = true, disable_coordinates = true }) end, desc = "Live grep" },

  { "<leader>b", group = "Buffers" },
  { "<leader>bb", function() telescope_builtin.buffers({ sort_mru = true }) end, desc = "Find buffer" },
  { "<leader>bD", "<cmd>bdelete! %<CR>", desc = "Force delete current buffer" },

  { "<leader>c", group = "Configurations"},
  { "<leader>cc", function() telescope_builtin.colorscheme({ enable_preview = true }) end, desc = "Choose colorscheme" },
  { "<leader>cH", "<cmd>call Helptags()<CR>", desc = "Generate helptags" },

  { "<leader>e", group = "Editor" },
  { "<leader>eb", '<cmd>Git blame<CR>', desc = 'Open git blame' },
  { "<leader>ec", group = "Case Convert" },
  { "<leader>ecc", group = "camelCase to ..." },
  { "<leader>ecch", desc = "hyphen-case" },
  { "<leader>eccs", desc = "snake_case" },
  { "<leader>ech", group = "hyphen-case to ..." },
  { "<leader>echc", desc = "camelCase" },
  { "<leader>echs", desc = "snake_case" },
  { "<leader>ecs", group = "snake_case to ..." },
  { "<leader>ecsc", desc = "camelCase" },
  { "<leader>ecsh", desc = "hyphen-case" },
  { "<leader>ef", group = "Focus" },
  { "<leader>eff", "<cmd>TZFocus<CR>", desc = "Toggle Focus mode" },
  { "<leader>efn", "<cmd>TZNarrow<CR>", desc = "Toggle Narrow mode" },
  { "<leader>efz", "<cmd>TZAtaraxis<CR>", desc = "Toggle Zen mode" },
  { "<leader>es", group = "Style" },
  { "<leader>est", ToggleTypewriterMode, desc = "Toggle Typewriter Mode" },
  { "<leader>esn", ToggleLineNumbers, desc = "Toggle Line Numbers" },
  { "<leader>esr", ToggleRelativeNumbers, desc = "Toggle Relative Numbers" },
  { "<leader>ess", group = "Signify" },
  { "<leader>esst", "<cmd>SignifyToggle<CR>", desc = "Toggle Signs" },
  { "<leader>essr", "<cmd>SignifyRefresh<CR>", desc = "Refresh Signs" },
  { "<leader>et", ":Tabularize /", desc = "Tabularize to..." },
  { "<leader>eT", group = "Template" },
  { "<leader>eTg", "<cmd>Denite -split=floating gettemplates<CR>", desc = "Get template" },
  { "<leader>eTe", "<cmd>Denite -split=floating edittemplates<CR>", desc = "Edit template" },
  { "<leader>ew", '<cmd>%s/\\s*$//e<CR>', desc = 'Delete trailing whitespaces' },

  { "<leader>f", desc = "Format" },

  { "<leader>g", group = "Git" },
  { "<leader>gh", function() telescope_builtin.git_bcommits() end, desc = "See file history" },
  { "<leader>gs", function() telescope_builtin.git_status() end, desc = "See git status" },
  { "<leader>gS", function() telescope_builtin.git_stash() end, desc = "See git stash" },

  { "<leader>j", function() telescope_builtin.find_files() end, desc = "Jump" },
  {
    "<leader>J",
    function() telescope_builtin.find_files({ hidden = true, no_ignore = true }) end,
    desc = "Jump with hiddens and ignored"
  },

  { "<leader>l", group = "LSP" },
  { "<leader>la", desc = "Code actions" },
  { "<leader>ld", "<cmd>Trouble diagnostics toggle filter.buf=0<CR>", desc = "Show buffer diags" },
  { "<leader>lD", "<cmd>Trouble diagnostics toggle<CR>", desc = "Show all diags" },
  { "<leader>lR", desc = "Rename" },
  { "<leader>ls", group = "Symbols search" },
  { "<leader>lss", function() telescope_builtin.treesitter() end, desc = "Just symbols" },
  { "<leader>lsd", function() telescope_builtin.lsp_document_symbols() end, desc = "Document" },
  { "<leader>lsw", function() telescope_builtin.lsp_workspace_symbols() end, desc = "Workspace" },
  { "<leader>lW", group = "Workspace folders" },
  { "<leader>lWa", desc = "Add workspace folder" },
  { "<leader>lWr", desc = "Remove workspace folder" },
  { "<leader>lWl", desc = "List workspace folders" },

  { "<leader>w", group = "Window" },
  { "<leader>wm", group = "Move" },
  { "<leader>wmH", "<C-W>H", desc = "To far left" },
  { "<leader>wmJ", "<C-W>J", desc = "To very bottom" },
  { "<leader>wmK", "<C-W>K", desc = "To very top" },
  { "<leader>wmL", "<C-W>L", desc = "To far right" },
  { "<leader>wmT", "<C-W>T", desc = "To new tab" },
  { "<leader>wr", group = "Resize" },
  { "<leader>wr=", "<C-W>=", desc = "Equalize" },
  { "<leader>wrh", "<C-W>5<", desc = "Expand left" },
  { "<leader>wrH", "<C-W>15<", desc = "Expand further left" },
  { "<leader>wrj", "5<C-W>+", desc = "Expand down" },
  { "<leader>wrJ", "15<C-W>+", desc = "Expand further down" },
  { "<leader>wrk", "5<C-W>-", desc = "Expand up" },
  { "<leader>wrK", "15<C-W>-", desc = "Expand further up" },
  { "<leader>wrl", "<C-W>5>", desc = "Expand right" },
  { "<leader>wrL", "<C-W>15>", desc = "Expand further right" },
  { "<leader>ws", group = "Horizontal split" },
  { "<leader>wsc", "<C-W>s", desc = "Current buffer" },
  { "<leader>wse", ":split<Space>", desc = "Edit file..." },
  { "<leader>wsd", "<C-W>d", desc = "Goto defn" },
  { "<leader>wsf", "<C-W>f", desc = "Edit filename under cursor" },
  { "<leader>wsi", "<C-W>i", desc = "Edit identifier" },
  { "<leader>wsn", "<cmd>new<CR>", desc = "New window" },
  { "<leader>wst", "<cmd>split term://zsh<CR>", desc = "New terminal" },
  { "<leader>wt", group = "Tabs" },
  { "<leader>wtc", "<C-W>T", desc = "Current buffer" },
  { "<leader>wte", ":tabedit<Space>", desc = "Edit file..." },
  { "<leader>wtf", "<C-W>gF", desc = "Edit filename under cursor" },
  { "<leader>wtn", "<cmd>tabnew<CR>", desc = "New window" },
  { "<leader>wtt", "<cmd>tabnew term://zsh<CR>", desc = "New terminal" },
  { "<leader>wv", group = "Vertical split" },
  { "<leader>wvc", "<C-W>v", desc = "Current buffer" },
  { "<leader>wve", ":vsplit<Space>", desc = "Edit file..." },
  { "<leader>wvn", "<cmd>:vnew<CR>", desc = "New window" },
  { "<leader>wvt", "<cmd>vsplit term://zsh<CR>", desc = "New terminal" },
  { "<leader>ww", "<cmd>call WindowSwap#EasyWindowSwap()<CR>", desc = "Window swap" },

  { "gc", group = "Comment out" },
  { "gcc", desc = "Current line" },
  { "gc{", desc = "Till last empty line" },
  { "gc}", desc = "Till next empty line" },
  { "gcG", desc = "Till end of file" },

  { "gd", function() telescope_builtin.lsp_definitions() end, desc = "Goto Definition" },
  { "gD", desc = "Goto Declaration" },
  { "gF", desc = "Goto Type Definition" },
  { "gi", desc = "Goto Implementation" },
  { "gr", desc = "Goto References" },

  { "m", group = "Move" },
  { "mt", group = "Tabs" },
  { "mt[", "To first" },
  { "mt]", "To last" },
  { "mtn", "To next" },
  { "mtp", "To prev" },

  -- Debugging
  { "<F5>", function() dap.continue() end, desc = "Continue" },
  { "<F10>", function() dap.step_over() end, desc = "Step Over" },
  { "<F11>", function() dap.step_into() end, desc = "Step Into" },
  { "<F12>", function() dap.step_out() end, desc = "Step Out" },
  { "<C-w>h", "<cmd>wincmd h<cr>", desc = "Go to the left window" },
  { "<C-w>j", "<cmd>wincmd j<cr>", desc = "Go to the window below" },
  { "<C-w>k", "<cmd>wincmd k<cr>", desc = "Go to the window above" },
  { "<C-w>l", "<cmd>wincmd l<cr>", desc = "Go to the right window" },
  { "<leader>D", group = "Debugger" },
  { "<leader>Db", function() dap.toggle_breakpoint() end, desc = "Toggle breakpoint" },
  { "<leader>DB", function() dap.set_breakpoint(vim.fn.input('Breakpoint condition: ')) end, desc = "Set conditional breakpoint" },
  { "<leader>Dl", function() dap.set_breakpoint(nil, nil, vim.fn.input('Log point message: ')) end, desc = "Add log point breakpoint" },
  { "<leader>Dr", function() dap.repl.toggle() end, desc = "Toggle REPL" },
  { "<leader>DR", function() dap.run_last() end, desc = "Run last debugging session" },
  {
    "<leader>Ds",
    function()
      if dap.session() == nil then
        print("There aren't any active DAP sessions")
      else
        print("There's an active DAP session")
      end
    end,
    desc = "Show session"
  },
  {
    "<leader>DS",
    function()
      dap.terminate()
      dap.close()
    end,
    desc = "Stop debugger"
  },
  { "<leader>Dv", group = "View" },
  { "<leader>Dvs", function() dap_widgets.sidebar(dap_widgets.scopes).open() end, desc = "Open scopes view in sidebar" },
  { "<leader>Dvf", function() dap_widgets.sidebar(dap_widgets.frames).open() end, desc = "Open frames view in sidebar" },
  { "<leader>Dvv", function() dap_widgets.hover().toggle() end, desc = "View value" },

  {
    mode = { "v" },
    { "<leader>c", group = "Change case" },
    { "<leader>cc", group = "From camelCase..." },
    { "<leader>cch", "<cmd>CamelToHyphenSel<CR>", desc = "... to hyphen-case" },
    { "<leader>ccs", "<cmd>CamelToSnakeSel<CR>", desc = "... to snake_case" },
    { "<leader>ch", group = "From hyphen-case..." },
    { "<leader>chc", "<cmd>HyphenToCamelSel<CR>", desc = "... to camelCase" },
    { "<leader>chs", "<cmd>HyphenToSnakeSel<CR>", desc = "... to snake_case" },
    { "<leader>cs", group = "From snake_case..." },
    { "<leader>csc", "<cmd>SnakeToCamelSel<CR>", desc = "... to camelCase" },
    { "<leader>csh", "<cmd>SnakeToHyphenSel<CR>", desc = "... to hyphen-case" },

    { "<leader>e", group = "Editor" },
    { "<leader>ef", group = "Focus" },
    { "<leader>efn", "<cmd>TZNarrow<CR>", desc = "Toggle Narrow mode" },
  },
})

whichkey.setup {
  replace = {
    ["<leader>"] = "",
    ["<space>"] = "",
  },
  icons = {
    group = "≫ ",
  },
}
