require 'lsp_servers'
require 'dap_setups'
require 'whichkey'
require 'treesitter-settings'

vim.diagnostic.config({
  virtual_text = false,
})

local cmp = require 'cmp'
cmp.setup {
  enabled = true,
  completion = {
    autocompletion = true,
  },
  snippet = {
    expand = function(args)
      vim.fn["vsnip#anonymous"](args.body)
    end,
  },
  mapping = {
    ['<C-p>'] = cmp.mapping.select_prev_item(),
    ['<C-n>'] = cmp.mapping.select_next_item(),
    ['<C-d>'] = cmp.mapping.scroll_docs(-4),
    ['<C-f>'] = cmp.mapping.scroll_docs(4),
    ['<M-Space>'] = cmp.mapping.complete(),
    ['<C-e>'] = cmp.mapping.close(),
    ['<CR>'] = cmp.mapping.confirm {
      behavior = cmp.ConfirmBehavior.Replace,
      select = true,
    },
    ['<Tab>'] = function(fallback)
      if cmp.visible() then
        cmp.select_next_item()
      else
        fallback()
      end
    end,
    ['<S-Tab>'] = function(fallback)
      if cmp.visible() then
        cmp.select_prev_item()
      else
        fallback()
      end
    end,
  },
  sources = {
    { name = "path" },
    { name = 'nvim_lsp' },
    { name = 'vsnip' },
  },
}

require "lsp_signature".setup({
  bind = true,
  hint_prefix = "🦆 ",
  handler_opts = {
    border = "single",
  },
})

require("trouble").setup {
  icons = false,
  use_diagnostic_signs = true
}

require("true-zen").setup {
  integrations = {
    twilight = true,
  },
}
