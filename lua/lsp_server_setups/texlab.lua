local nvim_lsp = require 'lspconfig'
local on_attach = require "setup_confs/on_attach"
local BaseConfig = require 'setup_confs/general'

local setup_config = BaseConfig.extend_base_configs({
  on_attach = on_attach,
  cmd = { "texlab" },
  filetypes = { "tex", "bib" },
  root_dir = nvim_lsp.util.root_pattern(".latexmkrc", ".git"),
})

nvim_lsp.texlab.setup(setup_config)
