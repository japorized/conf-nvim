local nvim_lsp = require 'lspconfig'
local BaseConfig = require 'setup_confs/general'

local setup_config = BaseConfig.extend_base_configs({
  cmd = { "clangd" },
  filetypes = { "c", "cpp", "objc", "objcpp", "cuda" },
  root_dir = nvim_lsp.util.root_pattern(".clangd", ".clang-tidy", ".clang-format", "compile_commands.json",
    "compile_flags.txt", "configure.ac", ".git"),
  single_file_support = true,
})

nvim_lsp.clangd.setup(setup_config)
