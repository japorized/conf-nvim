local nvim_lsp = require 'lspconfig'
local BaseConfig = require 'setup_confs/general'

local setup_config = BaseConfig.extend_base_configs({
  cmd = { "haskell-language-server-wrapper", "--lsp" },
  filetypes = { "haskell", "lhaskell" },
  root_dir = function(filepath)
    return (
        nvim_lsp.util.root_pattern('hie.yaml', 'stack.yaml', 'cabal.project')(filepath)
            or nvim_lsp.util.root_pattern('*.cabal', 'package.yaml')(filepath)
        )
  end,
  settings = {
    formattingProvider = "ormolu"
  },
})

nvim_lsp.hls.setup(setup_config)
