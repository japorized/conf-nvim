local nvim_lsp = require 'lspconfig'
local BaseConfig = require 'setup_confs/general'

local setup_config = BaseConfig.extend_base_configs({
  cmd = { "OmniSharp" },
  root_dir = function(file, _)
    if file:sub(-#".csx") == ".csx" then
      return nvim_lsp.util.path.dirname(file)
    end
    return nvim_lsp.util.root_pattern("*.sln")(file) or nvim_lsp.util.root_pattern("*.csproj")(file)
  end,
})

nvim_lsp.omnisharp.setup(setup_config)

