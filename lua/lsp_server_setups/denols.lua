local file_exists = require 'utils.file_exists'
local nvim_lsp = require 'lspconfig'
local BaseConfig = require 'setup_confs/general'

if not file_exists.on_relative_path('package.json') then
  local setup_config = BaseConfig.extend_base_configs({
    cmd = { "deno", "lsp" },
    filetypes = { "typescript", "javascript", "typescriptreact", "javascriptreact" },
    root_dir = nvim_lsp.util.root_pattern("tsconfig.json", "deno.jsonc", "deno.json", ".git"),
    init_options = {
      enable = true,
      lint = true,
    },
  })

  nvim_lsp.denols.setup(setup_config)
end

vim.g.markdown_fenced_languages = {
  "ts=typescript"
}
