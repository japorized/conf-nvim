local nvim_lsp = require 'lspconfig'
local BaseConfig = require 'setup_confs/general'

local setup_config = BaseConfig.extend_base_configs({
  cmd = { "gopls" },
  filetypes = { "go", "gomod", "gotmpl" },
  root_dir = nvim_lsp.util.root_pattern("go.mod", ".git"),
})

nvim_lsp.gopls.setup(setup_config)
