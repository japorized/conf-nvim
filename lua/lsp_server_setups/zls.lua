local nvim_lsp = require 'lspconfig'
local BaseConfig = require 'setup_confs/general'

local setup_config = BaseConfig.extend_base_configs({
  cmd = { "zls" },
  filetypes = { "zig", "zir" },
  root_dir = nvim_lsp.util.root_pattern("zls.json", ".git"),
  single_file_support = true,
})

nvim_lsp.zls.setup(setup_config)
