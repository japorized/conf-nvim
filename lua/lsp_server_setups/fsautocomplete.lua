local nvim_lsp = require 'lspconfig'
local BaseConfig = require 'setup_confs/general'

local setup_config = BaseConfig.extend_base_configs({})

nvim_lsp.fsautocomplete.setup(setup_config)

