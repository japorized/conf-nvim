local nvim_lsp = require 'lspconfig'
local BaseConfig = require 'setup_confs/general'
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities.textDocument.completion.completionItem.snippetSupport = true

local setup_config = BaseConfig.extend_base_configs({
  capabilities = capabilities,
  root_dir = nvim_lsp.util.root_pattern("*.css"),
  filetypes = { "css", "scss" },
})

nvim_lsp.cssls.setup(setup_config)

