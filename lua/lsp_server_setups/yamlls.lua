local nvim_lsp = require 'lspconfig'
local BaseConfig = require 'setup_confs/general'

local setup_config = BaseConfig.extend_base_configs({
  cmd = { "yaml-language-server", "--stdio" },
  filetypes = { "yaml", "yaml.docker-compose" },
  root_dir = nvim_lsp.util.find_git_ancestor,
  settings = {
    redhat = {
      telemetry = {
        enabled = false
      }
    }
  },
  single_file_support = true
})

nvim_lsp.yamlls.setup(setup_config)
