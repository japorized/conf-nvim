local nvim_lsp = require 'lspconfig'
local BaseConfig = require 'setup_confs/general'

local setup_config = BaseConfig.extend_base_configs({
  cmd = { "terraform-ls", "serve" },
  filetypes = { "tf" },
  root_dir = nvim_lsp.util.root_pattern(".git", ".terraform"),
})

nvim_lsp.terraformls.setup(setup_config)
