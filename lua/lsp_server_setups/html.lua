local nvim_lsp = require 'lspconfig'
local BaseConfig = require 'setup_confs/general'
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities.textDocument.completion.completionItem.snippetSupport = true

local setup_config = BaseConfig.extend_base_configs({
  capabilities = capabilities,
  root_dir = nvim_lsp.util.root_pattern("*.html"),
  filetypes = { "html", "njk", "jinja" },
})

nvim_lsp.html.setup(setup_config)
