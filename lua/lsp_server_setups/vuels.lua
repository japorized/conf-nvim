local nvim_lsp = require 'lspconfig'
local on_attach = require "setup_confs/on_attach"
local BaseConfig = require 'setup_confs/general'

local setup_config = BaseConfig.extend_base_configs({
  on_attach = function(client, bufnr)
    -- Init the general config
    on_attach(client, bufnr)

    -- See also
    -- https://github.com/neovim/nvim-lspconfig/issues/821
    -- https://vuejs.github.io/vetur/guide/formatting.html
    client.server_capabilities.documentFormattingProvider = false
  end,
  root_dir = function (fname)
    return nvim_lsp.util.root_pattern("vue.config.js", ".git")(fname) or -- for pure vue (and vite) projects
      nvim_lsp.util.root_pattern("nuxt.config.js", ".git")(fname) -- for nuxt projects
  end,
  init_options = {
    config = {
      vetur = {
        format = {
          defaultFormatter = {
            js = "prettier",
            ts = "prettier",
            html = "prettier",
            css = "prettier",
            less = "prettier",
            postcss = "prettier",
          },
          enable = true,
        }
      }
    }
  }
})

nvim_lsp.vuels.setup(setup_config)
