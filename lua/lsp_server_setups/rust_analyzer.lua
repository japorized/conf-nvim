local nvim_lsp = require 'lspconfig'
local on_attach = require 'setup_confs/on_attach'
local BaseConfig = require 'setup_confs/general'

local setup_config = BaseConfig.extend_base_configs({
  on_attach = on_attach,
  root_dir = nvim_lsp.util.root_pattern("Cargo.toml", ".git"),
  settings = {
    ["rust-analyzer"] = {
      imports = {
        granularity = {
          group = "module",
        },
        prefix = "self",
      },
      cargo = {
        buildScripts = {
          enable = true,
        },
      },
      procMacro = {
        enable = true,
      },
    },
  },
})

nvim_lsp.rust_analyzer.setup(setup_config)
