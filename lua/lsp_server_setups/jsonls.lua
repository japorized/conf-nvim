local nvim_lsp = require 'lspconfig'
local BaseConfig = require 'setup_confs/general'

local setup_config = BaseConfig.extend_base_configs({
  commands = {
    Format = {
      function()
        vim.lsp.buf.range_formatting({}, {0, 0}, {vim.fn.line('$'), 0})
      end
    }
  },
})

nvim_lsp.jsonls.setup(setup_config)
