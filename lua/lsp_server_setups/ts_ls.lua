local nvim_lsp = require 'lspconfig'
local BaseConfig = require 'setup_confs/general'
local on_attach = require 'setup_confs/on_attach'
local file_exists = require 'utils.file_exists'

if file_exists.on_relative_path("package.json") then
  local setup_config = BaseConfig.extend_base_configs({
    on_attach = on_attach,
    cmd = { "typescript-language-server", "--stdio" },
    filetypes = { "typescript", "javascript" },
    root_dir = nvim_lsp.util.root_pattern("node_modules", "package.json"),
  })

  nvim_lsp.ts_ls.setup(setup_config)
end
