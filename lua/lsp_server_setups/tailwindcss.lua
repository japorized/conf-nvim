local nvim_lsp = require 'lspconfig'
local BaseConfig = require 'setup_confs/general'

local setup_config = BaseConfig.extend_base_configs({
  root_dir = function (fname)
    return nvim_lsp.util.root_pattern("tailwind.config.js")(fname) or
      nvim_lsp.util.root_pattern("tailwind.config.ts")(fname)
  end,
})

nvim_lsp.tailwindcss.setup(setup_config)
