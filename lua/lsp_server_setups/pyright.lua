local nvim_lsp = require 'lspconfig'
local BaseConfig = require 'setup_confs/general'

local setup_config = BaseConfig.extend_base_configs({
  cmd = { "pyright-langserver", "--stdio" },
  filetypes = { "python" },
  root_dir = function(fname)
    local root_files = {
      'requirements.txt',
      'pyproject.toml',
    }
    return nvim_lsp.util.root_pattern(unpack(root_files))(fname) or nvim_lsp.util.find_git_ancestor(fname)
  end,
  single_file_support = true
})

nvim_lsp.pyright.setup(setup_config)
