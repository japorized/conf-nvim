local nvim_lsp = require 'lspconfig'
local on_attach = require "setup_confs/on_attach"
local BaseConfig = require 'setup_confs/general'

local setup_config = BaseConfig.extend_base_configs({
  on_attach = on_attach,
  cmd = { "svelteserver", "--stdio" },
  filetypes = { "svelte" },
  root_dir = nvim_lsp.util.root_pattern("package.json", ".git", "svelte.config.js"),
})

nvim_lsp.svelte.setup(setup_config)
