local nvim_lsp = require 'lspconfig'
local BaseConfig = require 'setup_confs/general'

local setup_config = BaseConfig.extend_base_configs({
  cmd = { "dart", "language-server" },
  filetypes = { "dart" },
  init_options = {
    closingLabels = true,
    flutterOutline = true,
    onlyAnalyzeProjectsWithOpenFiles = true,
    outline = true,
    suggestFromUnimportedLibraries = true
  },
  root_dir = nvim_lsp.util.root_pattern("pubspec.yaml"),
  settings = {
    dart = {
      completeFunctionCalls = true,
      showTodos = true
    }
  }
})

nvim_lsp.dartls.setup(setup_config)
