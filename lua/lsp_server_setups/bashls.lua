local nvim_lsp = require 'lspconfig'
local BaseConfig = require 'setup_confs/general'

local setup_config = BaseConfig.extend_base_configs({
  cmd = { "bash-language-server", "start" },
  cmd_env = {
    GLOB_PATTERN = "*@(.sh|.inc|.bash|.command)",
    INCLUDE_ALL_WORKSPACE_SYMBOLS = "true",
  },
  filetypes = { "sh" },
  root_dir = nvim_lsp.util.find_git_ancestor,
  single_file_support = true,
})

nvim_lsp.bashls.setup(setup_config)
