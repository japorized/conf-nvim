local nvim_lsp = require 'lspconfig'
local BaseConfig = require 'setup_confs/general'

local setup_config = BaseConfig.extend_base_configs({
  cmd = { "solargraph", "stdio" },
  root_dir = nvim_lsp.util.root_pattern("Gemfile"),
  init_options = {
    formatting = true,
  },
  settings = {
    solargraph = {
      diagnostics = true,
    }
  }
})

nvim_lsp.solargraph.setup(setup_config)

