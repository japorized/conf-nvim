local nvim_lsp = require 'lspconfig'
local on_attach = require "setup_confs/on_attach"
local BaseConfig = require 'setup_confs/general'

local setup_config = BaseConfig.extend_base_configs({
  cmd = {
    'julia',
    '--startup-file=no',
    '--history-file=no',
    '-e',
    [[
      # Load LanguageServer.jl: attempt to load from ~/.julia/environments/nvim-lspconfig
      # with the regular load path as a fallback
      ls_install_path = joinpath(
          get(DEPOT_PATH, 1, joinpath(homedir(), ".julia")),
          "environments", "nvim-lspconfig"
      )
      pushfirst!(LOAD_PATH, ls_install_path)
      using LanguageServer
      popfirst!(LOAD_PATH)
      depot_path = get(ENV, "JULIA_DEPOT_PATH", "")
      project_path = let
          dirname(something(
              ## 1. Finds an explicitly set project (JULIA_PROJECT)
              Base.load_path_expand((
                  p = get(ENV, "JULIA_PROJECT", nothing);
                  p === nothing ? nothing : isempty(p) ? nothing : p
              )),
              ## 2. Look for a Project.toml file in the current working directory,
              ##    or parent directories, with $HOME as an upper boundary
              Base.current_project(),
              ## 3. First entry in the load path
              get(Base.load_path(), 1, nothing),
              ## 4. Fallback to default global environment,
              ##    this is more or less unreachable
              Base.load_path_expand("@v#.#"),
          ))
      end
      @info "Running language server" VERSION pwd() project_path depot_path
      server = LanguageServer.LanguageServerInstance(stdin, stdout, project_path, depot_path)
      server.runlinter = true
      run(server)
    ]],
  },
  filetypes = { "julia" },
  on_attach = function(client, bufnr)
    -- Init the general config
    on_attach(client, bufnr)

    client.server_capabilities.documentFormattingProvider = true
  end,
  root_dir = function(fname)
    return nvim_lsp.util.root_pattern 'Project.toml'(fname) or nvim_lsp.util.find_git_ancestor(fname)
  end,
  single_file_support = true,
})

nvim_lsp.julials.setup(setup_config)
