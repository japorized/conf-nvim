local function file_exists(name)
  local f = io.open(name, 'r')
  if f ~= nil then io.close(f) return true else return false end
end

local function relative_file_exists(name)
  return file_exists(os.getenv('PWD') .. '/' .. name)
end

return {
  on_absolute_path = file_exists,
  on_relative_path = relative_file_exists,
}
