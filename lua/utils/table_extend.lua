local function is_table(ent)
  return (type(ent) == 'table')
end

local function table_extend(table1, ...)
  if (not is_table(table1)) then
    error("Cannot extend a non-table entity")
  end

  local tmp_table = table1
  local extensions = {...}
  for i = 1, #extensions do
    if (is_table(extensions[i])) then
      for k, v in pairs(extensions[i]) do tmp_table[k] = v end
    end
  end

  return tmp_table
end

return table_extend
