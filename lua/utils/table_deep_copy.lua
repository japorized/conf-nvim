return function(t)
    if type(t) ~= "table" then return t end
    local result = {}
    local stack = {[result] = t}
    while next(stack) do
        local new_stack = {}
        for target, current in next, stack do
            for k, v in next, current do
                if type(v) == "table" then
                    target[k] = {}
                    new_stack[target[k]] = current[k]
                else
                    target[k] = v
                end
            end
        end
        stack = new_stack
    end
    return result
end
