-- We'll use an on_attach function to only map the keybinds
-- after the language server attaches to the current buffer

local opts = { noremap=true, silent=true }
vim.api.nvim_set_keymap('n', 'E', '<cmd>lua vim.diagnostic.open_float()<CR>', opts)
vim.api.nvim_set_keymap('n', '[d', '<cmd>lua vim.diagnostic.goto_prev()<CR>', opts)
vim.api.nvim_set_keymap('n', ']d', '<cmd>lua vim.diagnostic.goto_next()<CR>', opts)
-- vim.api.nvim_set_keymap('n', '<space>q', '<cmd>lua vim.diagnostic.setloclist()<CR>', opts)

local on_attach = function(_client, bufnr)
  -- Enable completion triggered by <c-x><c-o>
  vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')

  -- Mappings.
  -- See `:help vim.lsp.*` for documentation on any of the below functions
  local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end

  buf_set_keymap('n'    , 'gD'         , '<cmd>lua vim.lsp.buf.declaration()<CR>'                                , opts)
  -- buf_set_keymap('n' , 'gd'         , '<cmd>lua vim.lsp.buf.definition()<CR>'                                 , opts)
  buf_set_keymap('n'    , 'K'          , '<cmd>lua vim.lsp.buf.hover()<CR>'                                      , opts)
  buf_set_keymap('n'    , 'gi'         , '<cmd>lua vim.lsp.buf.implementation()<CR>'                             , opts)
  -- buf_set_keymap('n' , '<C-s>'      , '<cmd>lua vim.lsp.buf.signature_help()<CR>'                             , opts)
  buf_set_keymap('n'    , '<space>lWa' , '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>'                       , opts)
  buf_set_keymap('n'    , '<space>lWr' , '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>'                    , opts)
  buf_set_keymap('n'    , '<space>lWl' , '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>' , opts)
  buf_set_keymap('n'    , 'gF'         , '<cmd>lua vim.lsp.buf.type_definition()<CR>'                            , opts)
  buf_set_keymap('n'    , '<space>lR'  , '<cmd>lua vim.lsp.buf.rename()<CR>'                                     , opts)
  buf_set_keymap('n'    , '<space>la'  , '<cmd>lua vim.lsp.buf.code_action()<CR>'                                , opts)
  buf_set_keymap('n'    , 'gr'         , '<cmd>Trouble lsp_references<CR>'                                       , opts)
  buf_set_keymap('n'    , '<space>f'   , '<cmd>lua vim.lsp.buf.format({ async = true })<CR>'                     , opts)
end

return on_attach
