local on_attach = require "setup_confs/on_attach"
local table_extend = require 'utils.table_extend'
local table_deep_copy = require 'utils.table_deep_copy'

local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = table_extend(capabilities, require("cmp_nvim_lsp").default_capabilities())

local base_setup_configs = {
  capabilities = capabilities,
  on_attach = on_attach,
  flags = {
    debounce_text_changes = 150,
  },
  handlers = {
    ["textDocument/publishDiagnostics"] = vim.lsp.with(
      vim.lsp.diagnostic.on_publish_diagnostics, {
        -- Disable virtual_text
        virtual_text = false
      }
    ),
  }
}

local BaseConfig = {
  extend_base_configs = function(t)
    return table_extend(table_deep_copy(base_setup_configs), t)
  end,
}

return BaseConfig
